<?php

use App\Http\Controllers\Api\V1\CommentController;
use App\Http\Controllers\Api\V1\PostController;
use App\Http\Controllers\Api\V1\UserController;
use App\Http\Controllers\Api\V1\VideoController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix'=>'v1'],function (){
    Route::get('/user/{user}', [UserController::class, 'show']);
    Route::post('/user', [UserController::class, 'store']);

    Route::get('/post/{post}', [PostController::class, 'show']);
    Route::post('/post', [PostController::class, 'store'])->middleware('user');

    Route::get('/video/{video}', [VideoController::class, 'show']);
    Route::post('/video', [VideoController::class, 'store'])->middleware('user');

    Route::post('/{type}/{id}/comment', [CommentController::class, 'comment'])->middleware('user');
});

