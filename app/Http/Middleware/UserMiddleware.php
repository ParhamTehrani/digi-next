<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class UserMiddleware
{

    public function handle(Request $request, Closure $next)
    {
        if ($request->header('User-ID')){
            return $next($request);
        }else{
            abort(403);
        }
    }
}
