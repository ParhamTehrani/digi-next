<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\PostCreateRequest;
use App\Http\Resources\Api\PostCreateResource;
use App\Http\Resources\Api\PostShowResource;
use App\Services\Interfaces\PostServiceInterface;

class PostController extends Controller
{
    public $post;
    public function __construct(PostServiceInterface $post)
    {
        $this->post = $post;
    }
    public function show($id)
    {
        $post = $this->post->getPostById($id);

        return response()->json(PostShowResource::make($post));
    }

    public function store(PostCreateRequest $request)
    {
        $validatedData = $request->validated();

        $post = $this->post->storePost($validatedData);

        return response()->json(PostCreateResource::make($post),201);
    }
}
