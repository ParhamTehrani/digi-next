<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\UserCreateRequest;
use App\Http\Resources\Api\UserCreateResource;
use App\Http\Resources\Api\UserShowResource;
use App\Services\Interfaces\UserServiceInterface;
class UserController extends Controller
{
    public $user;
    public function __construct(UserServiceInterface $user)
    {
        $this->user = $user;
    }
    public function show($id)
    {
        $user = $this->user->getUserById($id);

        return response()->json(UserShowResource::make($user));
    }

    public function store(UserCreateRequest $request)
    {
        $validatedData = $request->validated();

        $user = $this->user->storeUser($validatedData);

        return response()->json(UserCreateResource::make($user),201);
    }
}
