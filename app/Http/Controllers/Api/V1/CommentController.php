<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\CommentCreateRequest;
use App\Http\Resources\Api\CommentCreateResource;
use App\Services\Interfaces\CommentServiceInterface;

class CommentController extends Controller
{
    public $comment;
    public function __construct(CommentServiceInterface $comment)
    {
        $this->comment = $comment;
    }

    public function comment(CommentCreateRequest $request)
    {
        $validateData = $request->validated();

        $comment = $this->comment->storeCommentByType($validateData);

        return response()->json(CommentCreateResource::make($comment));
    }
}
