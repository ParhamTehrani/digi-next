<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\VideoCreateRequest;
use App\Http\Resources\Api\VideoCreateResource;
use App\Http\Resources\Api\VideoShowResource;
use App\Services\Interfaces\VideoServiceInterface;

class VideoController extends Controller
{
    public $video;
    public function __construct(VideoServiceInterface $video)
    {
        $this->video = $video;
    }
    public function show($id)
    {
        $video = $this->video->getVideoById($id);

        return response()->json(VideoShowResource::make($video));
    }

    public function store(VideoCreateRequest $request)
    {
        $validatedData = $request->validated();

        $video = $this->video->storeVideo($validatedData);

        return response()->json(VideoCreateResource::make($video),201);
    }
}
