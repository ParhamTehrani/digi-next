<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;

class PostShowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data = parent::toArray($request);

        $comments = [];
        foreach ($data['comment'] as $comment){
            $comments[] = [
                'text' => $comment['text'],
                'username' => $comment['user']['username']
            ];
        }

        return [
            'title' => $data['title'],
            'content' => $data['content'],
            'username' => $data['user']['username'],
            'comments' => $comments
        ];
    }
}
