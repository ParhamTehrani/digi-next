<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class CommentCreateRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $idValidation = 'required|';
        if (request()->type == 'post'){
            $idValidation .= 'exists:posts';
        }elseif(request()->type == 'video'){
            $idValidation .= 'exists:videos';
        }
        return [
            'type' => 'required|in:post,video',
            'id' => $idValidation,
            'text' => 'required|max:191'
        ];
    }

    public function all($keys = null)
    {
        return array_merge(parent::all(), $this->route()->parameters());
    }
}
