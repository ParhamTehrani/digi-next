<?php


namespace App\Services\Interfaces;


interface CommentServiceInterface
{
    public function storeCommentByType($payload);
}
