<?php


namespace App\Services\Interfaces;


interface VideoServiceInterface
{
    public function storeVideo($payload);

    public function getVideoById($id);
}
