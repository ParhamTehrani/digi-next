<?php


namespace App\Services\Interfaces;


interface PostServiceInterface
{
    public function storePost($payload);

    public function getPostById($id);
}
