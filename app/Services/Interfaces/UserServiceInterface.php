<?php


namespace App\Services\Interfaces;


interface UserServiceInterface
{
    public function storeUser($payload);

    public function getUserById($id);
}
