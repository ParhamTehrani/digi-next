<?php


namespace App\Services\Services;


use App\Repositories\Interfaces\VideoRepositoryInterface;
use App\Services\Interfaces\VideoServiceInterface;

class VideoService implements VideoServiceInterface
{

    public $video;
    public function __construct(VideoRepositoryInterface $video)
    {
        $this->video = $video;
    }


    public function storeVideo($payload)
    {
        $payload['user_id'] = request()->header('User-ID');

        return $this->video->create($payload);
    }

    public function getVideoById($id)
    {
        return $this->video->findById($id,['*'],['user','comment.user']);
    }
}
