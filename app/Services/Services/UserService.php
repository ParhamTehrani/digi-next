<?php


namespace App\Services\Services;


use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Services\Interfaces\UserServiceInterface;

class UserService implements UserServiceInterface
{

    public $user;
    public function __construct(UserRepositoryInterface $user)
    {
        $this->user = $user;
    }


    public function storeUser($payload)
    {
        return $this->user->create($payload);
    }

    public function getUserById($id)
    {
        return $this->user->findById($id);
    }
}
