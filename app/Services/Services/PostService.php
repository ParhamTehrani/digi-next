<?php


namespace App\Services\Services;


use App\Repositories\Interfaces\PostRepositoryInterface;
use App\Services\Interfaces\PostServiceInterface;

class PostService implements PostServiceInterface
{

    public $post;
    public function __construct(PostRepositoryInterface $post)
    {
        $this->post = $post;
    }

    public function storePost($payload)
    {
        $payload['user_id'] = request()->header('User-ID');

        return $this->post->create($payload);
    }

    public function getPostById($id)
    {
        return $this->post->findById($id,['*'],['user','comment.user']);
    }
}
