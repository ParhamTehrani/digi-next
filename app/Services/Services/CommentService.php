<?php


namespace App\Services\Services;


use App\Repositories\Interfaces\CommentRepositoryInterface;
use App\Repositories\Interfaces\PostRepositoryInterface;
use App\Repositories\Interfaces\VideoRepositoryInterface;
use App\Services\Interfaces\CommentServiceInterface;

class CommentService implements CommentServiceInterface
{

    public $comment;
    public $post;
    public $video;
    public function __construct(CommentRepositoryInterface $comment,VideoRepositoryInterface $video,PostRepositoryInterface $post)
    {
        $this->comment = $comment;
        $this->post = $post;
        $this->video = $video;
    }


    public function storeCommentByType($payload)
    {
        $data['text'] = $payload['text'];
        $data['user_id'] = request()->header('User-ID');

        if ($payload['type'] == 'post'){
            return $this->post->makePostComment($payload['id'],$data);
        } elseif($payload['type'] == 'video'){
            return $this->video->makeVideoComment($payload['id'],$data);
        }
    }
}
