<?php


namespace App\Repositories\Repositories;


use App\Repositories\Interfaces\BaseRepositoryInterface;

class BaseRepository implements BaseRepositoryInterface
{
    protected $model;

    public function __construct($model)
    {
        $this->model = $model;
    }

    public function all(array $columns = ['*'], array $relations = [])
    {
        return $this->model->with($relations)->withCount($relations)->get($columns);
    }

    public function findById(int $modelId, array $columns = ['*'], array $relations = [])
    {
        return $this->model->select($columns)->with($relations)->findOrFail($modelId);
    }

    public function create(array $payload)
    {
        $model = $this->model->create($payload);

        return $model->fresh();
    }

    public function update(int $modelId, array $payload)
    {
        $model = $this->findById($modelId);

        return $model->update($payload);
    }

    public function deleteById(int $modelId)
    {
        return $this->findById($modelId)->delete();
    }

    public function take(int $number, array $columns = ['*'], array $relations = [])
    {
        return $this->model->with($relations)->withCount($relations)->select($columns)->take($number)->get();
    }

    public function createAll(array $payload)
    {
        return $this->model->insert($payload);
    }
}
