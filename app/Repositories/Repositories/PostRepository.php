<?php


namespace App\Repositories\Repositories;


use App\Models\Post;
use App\Repositories\Interfaces\BaseRepositoryInterface;
use App\Repositories\Interfaces\PostRepositoryInterface;


class PostRepository extends BaseRepository implements PostRepositoryInterface
{
    protected $model;

    public function __construct(Post $video)
    {
        $this->model = $video;
    }

    public function makePostComment($id,$payload)
    {
        return $this->findById($id)->comment()->create($payload);
    }


}
