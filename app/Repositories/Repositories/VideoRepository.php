<?php


namespace App\Repositories\Repositories;


use App\Models\Video;
use App\Repositories\Interfaces\BaseRepositoryInterface;
use App\Repositories\Interfaces\VideoRepositoryInterface;


class VideoRepository extends BaseRepository implements VideoRepositoryInterface
{
    protected $model;

    public function __construct(Video $video)
    {
        $this->model = $video;
    }

    public function makeVideoComment($id,$payload)
    {
        return $this->findById($id)->comment()->create($payload);
    }

}
