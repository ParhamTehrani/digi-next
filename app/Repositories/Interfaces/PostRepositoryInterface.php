<?php


namespace App\Repositories\Interfaces;


interface PostRepositoryInterface extends BaseRepositoryInterface
{
    public function makePostComment($id,$payload);

}
